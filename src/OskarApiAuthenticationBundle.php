<?php
namespace Oskar\ApiAuthenticationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Oskar\ApiAuthenticationBundle\DependencyInjection\OskarApiAuthenticationExtension;

class OskarApiAuthenticationBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = new OskarApiAuthenticationExtension();
        }
        return $this->extension;
    }
}
