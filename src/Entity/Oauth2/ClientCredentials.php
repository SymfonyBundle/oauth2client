<?php
namespace Oskar\ApiAuthenticationBundle\Entity\Oauth2;


use Symfony\Component\Security\Core\User\UserInterface;

class ClientCredentials implements UserInterface
{

    /**
     * Undocumented variable
     *
     * @var string
     */
    private $clientId;

    /**
     * Undocumented variable
     *
     * @var string
     */
    private $clientSecret;

    /**
     * Undocumented variable
     *
     * @var string
     */
    private $grantType;


    public function __construct(
        string $clientId,
        string $client_secret,
        string $grant_type
    )
    {
        $this->clientId = $clientId;
        $this->clientSecret = $client_secret;
        $this->grantType = $grant_type;
    }

    /**
     * Get undocumented variable
     *
     * @return  string
     */ 
    public function getGrantType()
    {
        return $this->grantType;
    }

    /**
     * Set undocumented variable
     *
     * @param  string  $grantType  Undocumented variable
     *
     * @return  self
     */ 
    public function setGrantType(string $grantType)
    {
        $this->grantType = $grantType;

        return $this;
    }

    /**
     * Get undocumented variable
     *
     * @return  string
     */ 
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * Set undocumented variable
     *
     * @param  string  $clientSecret  Undocumented variable
     *
     * @return  self
     */ 
    public function setClientSecret(string $clientSecret)
    {
        $this->clientSecret = $clientSecret;

        return $this;
    }

    /**
     * Get undocumented variable
     *
     * @return  string
     */ 
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set undocumented variable
     *
     * @param  string  $clientId  Undocumented variable
     *
     * @return  self
     */ 
    public function setClientId(string $clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getRoles():array
    {
        return ['SERVICE'];
    }

    public function getUserIdentifier():string
    {
        return $this->clientId;
    }

    public function eraseCredentials(){
        return ;
    }

    public function getUsername(){
        return $this->clientId.' / '. $this->clientSecret;
    }


}
