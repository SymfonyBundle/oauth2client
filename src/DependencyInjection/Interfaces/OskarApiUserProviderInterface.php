<?php
namespace Oskar\ApiAuthenticationBundle\DependencyInjection\Interfaces;
use Symfony\Component\Security\Core\User\UserProviderInterface;

interface OskarApiUserProviderInterface extends UserProviderInterface
{
    public function getApiKey():string;
}
