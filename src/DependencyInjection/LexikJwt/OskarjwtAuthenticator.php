<?php
namespace Oskar\ApiAuthenticationBundle\DependencyInjection\LexikJwt;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidPayloadException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\TokenExtractorInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authenticator\JWTAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Oskar\ApiAuthenticationBundle\DependencyInjection\Interfaces\OskarApiUserProviderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;

class OskarjwtAuthenticator extends JWTAuthenticator
{
    public function __construct(JWTTokenManagerInterface $jwtManager,
                                EventDispatcherInterface $eventDispatcher,
                                TokenExtractorInterface $tokenExtractor,
                                OskarApiUserProviderInterface $userProvider,
                                AuthenticationSuccessHandler $successHandler)
    {
        parent::__construct($jwtManager,$eventDispatcher,$tokenExtractor,$userProvider);
        $this->userProvider = $userProvider;
        $this->jwtManager = $jwtManager;
        $this->successHandler = $successHandler;
    }

    public function supports(Request $request): ?bool
    {
        return false === $this->getTokenExtractor()->extract($request);
    }
    
    /**
     * Loads the user to authenticate.
     *
     * @param array                 $payload      The token payload
     * @param string                $identity     The key from which to retrieve the user "identifier"
     */
    protected function loadUser(array $payload, string $identity): UserInterface
    {
        return $this->userProvider->loadUserByIdentifier($identity);
    }

    public function doAuthenticate(Request $request)
    {

        $token = $this->getTokenExtractor()->extract($request);
        if(!$token){
            $data = json_decode($request->getContent(), true);
            
            if(!isset($data['apikey'])){
                throw new HttpException(400,"Waiting for Apikey!");
            }
            
            $userToAuthenticate = $this->userProvider->loadUserByIdentifier($data['apikey']);

            // create Token 
            $token = $this->jwtManager->create($userToAuthenticate);
        }

        try {
            if (!$payload = $this->jwtManager->parse($token)) {
                // check if the apiKey correct is an 
                throw new InvalidTokenException('Invalid JWT Token');
            }
        } catch (JWTDecodeFailureException $e) {
            if (JWTDecodeFailureException::EXPIRED_TOKEN === $e->getReason()) {
                throw new ExpiredTokenException();
            }

            throw new InvalidTokenException('Invalid JWT Token', 0, $e);
        }

        $idClaim = $this->jwtManager->getUserIdClaim();
        if (!isset($payload[$idClaim])) {
            throw new InvalidPayloadException($idClaim);
        }

        $passport = new SelfValidatingPassport(
            new UserBadge($payload[$idClaim],
            function ($userIdentifier) use($payload) {
                return $this->loadUser($payload, $userIdentifier);
            })
        );

        $passport->setAttribute('payload', $payload);
        $passport->setAttribute('token', $token);
        
        return $passport;

    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return $this->successHandler->onAuthenticationSuccess($request, $token);
    }

}
