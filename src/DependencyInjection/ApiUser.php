<?php
namespace Oskar\ApiAuthenticationBundle\DependencyInjection;

use Symfony\Component\Security\Core\User\UserInterface;

class ApiUser implements UserInterface
{

    private $apikey;
    
    
    public function getUserIdentifier():string
    {
        return $this->apikey;
    }


    public function getRoles():array
    {
        return ['IS_AUTHENTICATED_FULLY'];
    }

    /**
     * Get the value of apiToken
     */ 
    public function getApikey()
    {
        return $this->apikey;
    }

    /**
     * Set the value of apiToken
     *
     * @return  self
     */ 
    public function setApikey($apikey)
    {
        $this->apikey = $apikey;

        return $this;
    }


    public function eraseCredentials(){
        return ;
    }

    public function getUsername(){
        return $this->apikey;
    }
}
