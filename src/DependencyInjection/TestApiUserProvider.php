<?php

namespace Oskar\ApiAuthenticationBundle\DependencyInjection;

use Oskar\ApiAuthenticationBundle\DependencyInjection\ApiUserProvider;

class TestApiUserProvider extends ApiUserProvider
{
    public function __construct($apiKey="testApikey")
    {
        $this->apiKey = $apiKey;   
    }

    public function getApiKey(): string
    {
        parent::getApiKey();
        return $this->apiKey;
    }
}
