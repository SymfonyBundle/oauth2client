<?php

namespace Oskar\ApiAuthenticationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder("oskar_api_authentication");
        $rootNode = $treeBuilder->getRootNode();
        
        $rootNode
            ->children()
                ->arrayNode('api')
                    ->children()
                        ->scalarNode('api_key')->end()
                    ->end()
                ->end()
                ->arrayNode('oauth2')
                    ->children()
                        ->scalarNode('client_id')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('client_secret')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('grant_type')->defaultValue('client_credentials')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
