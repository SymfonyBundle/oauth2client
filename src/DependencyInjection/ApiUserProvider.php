<?php
namespace Oskar\ApiAuthenticationBundle\DependencyInjection;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiUserProvider implements UserProviderInterface
{
    private $apiKey;

    public function __construct(string $api_key)
    {
        $this->apiKey = $api_key;
    }

    /*
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the user is not supported
     * @throws UserNotFoundException    if the user is not found
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof ApiUser) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        
        if($user->getApikey()!==  $this->getApiKey()){
            throw new UserNotFoundException("api key", $user->getApikey());
        }
        
        return $user;
    }

    /*
     * @return bool
     */
    public function supportsClass(string $class): bool
    {
        return ApiUser::class === $class || is_subclass_of($class, ApiUser::class);
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        if($this->getApiKey() !== $identifier){
            throw new UserNotFoundException("the given apiKey is wrong: ". $identifier);
        }
        
        $user = new ApiUser();
        $user->setApikey($this->getApiKey());

        return $user;
    }
    
    public function getApiKey(): string
    {
        return $this->apiKey;
    }
}
