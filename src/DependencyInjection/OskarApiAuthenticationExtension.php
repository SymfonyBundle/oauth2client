<?php

namespace Oskar\ApiAuthenticationBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class OskarApiAuthenticationExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader($container, new FileLocator(dirname(__DIR__).'/Resources/config'));
        $loader->load('services.xml');

        $configuration = $this->getConfiguration($configs,$container);
        $config = $this->processConfiguration($configuration, $configs);


        $definition = $container->getDefinition('oskar_api_authentication.user_provider');
        $definition->setArgument(0, $config['api']['api_key']);

        $definition = $container->getDefinition('oskar_oauth_authentication.client_credentials_provider');
        $definition->setArgument(0, $config['oauth2']['client_id']);
        $definition->setArgument(1, $config['oauth2']['client_secret']);
        $definition->setArgument(2, $config['oauth2']['grant_type']);

        # Dynanically ad argument        
        /*         if (isset($config['api']['api_key'])){
            // $container->setAlias('oskar_api_authentication.api_user_provider', $config['api']['user_provider']);
            $container->setAlias('oskar_api_authentication.user_provider', $config['api']['api_key']);
        } */

    }

    public function getAlias(): string
    {
        return 'oskar_api_authentication';
    }

    public function getNamespace(): string
    {
        return 'oskar_api_authentication';
    }

    public function getXsdValidationBasePath()
    {
        return dirname(__DIR__).'/Resources/config/';
    }
}
