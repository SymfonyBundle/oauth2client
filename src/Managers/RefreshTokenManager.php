<?php
namespace Oskar\ApiAuthenticationBundle\Managers;

use Doctrine\ODM\MongoDB\DocumentManager;
use App\Document\Security\JwtRefreshToken;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;


class RefreshTokenManager implements RefreshTokenManagerInterface 
{
    
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function create(): RefreshTokenInterface
    {
        dd(" RefreshTokenManager create(): RefreshTokenInterface");
        return new JwtRefreshToken();
    }

    public function get($refreshToken): ?RefreshTokenInterface
    {
        $JwtRefreshToken = $this->dm->getRepository(JwtRefreshToken::class)->findOneBy(['refreshToken' => $refreshToken]);
       
        return $JwtRefreshToken;
    }

    public function getLastFromUsername($username):RefreshTokenInterface
    {
        dd(" RefreshTokenManager getLastFromUsername(): RefreshTokenInterface");
        return new JwtRefreshToken();
    }

    public function save(RefreshTokenInterface $refreshToken): void
    {

        $this->dm->persist($refreshToken);
        $this->dm->flush();
    }

    public function delete(RefreshTokenInterface $refreshToken, $andFlush = true):void
    {
        $this->dm->remove($refreshToken);

        if ($andFlush) {
            $this->objectManager->flush();
        }
    }

    public function revokeAllInvalid($datetime = null, $andFlush = true): array
    {
        $invalidTokens = $this->dm->getRepository(JwtRefreshToken::class)->findInvalid($datetime);

        foreach ($invalidTokens as $invalidToken) {
            $this->objectManager->remove($invalidToken);
        }

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return $invalidTokens;
    }

    /**
     * Returns the fully qualified class name for a concrete RefreshTokenInterface class.
     *
     * @return class-string<RefreshTokenInterface>
     */
    public function getClass()
    {
        return get_class(new JwtRefreshToken());
    }
}
