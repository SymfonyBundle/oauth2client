<?php
namespace Oskar\ApiAuthenticationBundle\Document;

use DateTimeInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenInterface;

/**
 * This class extends Gesdinet\JWTRefreshTokenBundle\Document\RefreshToken to have another collection name.
 *
 * @MongoDB\Document(collection="jwt_refresh_token")
 */
class JwtRefreshToken implements RefreshTokenInterface
{
    /**
     * 
     * @MongoDB\Id(strategy="INCREMENT", type="int")
     * @var int
     */
    protected $id;

    /**
     * 
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $refreshToken;

    /**
     * @MongoDB\Field(type="string")
     * @var string|null
     */
    protected $username;

    /**
     * @MongoDB\Field(type="date")
     * @var \DateTimeInterface|null
     */
    protected $valid;


    public static function createForUserWithTtl(string $refreshToken, UserInterface $user, int $ttl): RefreshTokenInterface
    {
        $jwtRefreshToken = new JwtRefreshToken();

        $jwtRefreshToken->setRefreshToken($refreshToken)
                        ->setUsername($user->getUserIdentifier());
        
        $expirationDate = new \DateTime();
        date_default_timezone_set('Europe/Berlin');
        $expirationDate->modify(sprintf('+%d seconds', $ttl));
        $jwtRefreshToken->setValid($expirationDate);

       return $jwtRefreshToken;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @param string|null $refreshToken
     * @return $this
     */
    public function setRefreshToken($refreshToken = null)
    {
        $this->refreshToken = $refreshToken;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @param \DateTimeInterface|null $valid
     *
     * @return $this
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getValid():DateTimeInterface
    {
        return $this->valid;
    }

    
    /**
     * @return string|null
     */
    public function getUsername(): string
    { 
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        date_default_timezone_set('Europe/Berlin');
        $now = new \DateTime('now');

        return $this->valid > $now;
    }

    public function __toString(): string
    {
        return " ";
    }
}
