<?php
namespace Oskar\ApiAuthenticationBundle\Services;

use Oskar\ApiAuthenticationBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiUserProvider implements UserProviderInterface
{

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        if($this->apiKey !== $identifier){
            throw new UserNotFoundException("the given apikey is wrong: ". $identifier);
        }
        
        $user = new User();
        $user->setApikey($this->apiKey);

        return $user;
    }

    public function supportsClass(string $class)
    {
        return User::class === $class || is_subclass_of($class, User::class);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        
        if($user->getApikey()!==  $this->apiKey){
            throw new UserNotFoundException("api key", $user->getApikey());
        }
        
        return $user;
    }

}
