<?php
namespace Oskar\ApiAuthenticationBundle\Services;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Oskar\ApiAuthenticationBundle\Entity\Oauth2\ClientCredentials;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class OauthCredentialProvider implements UserProviderInterface
{

    public function __construct(
        string $clientId,
        string $clientSecret,
        string $grantType
    )
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->grantType = $grantType;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        list($client_Id, $client_secret, $grant_type) =array_pad(explode(":", "$identifier: "), 3, null);

        if(($this->clientId === $client_Id || $this->clientSecret === $client_secret) !== true){
            throw new UserNotFoundException("the given clientId or client_secret  is wrong: ". $identifier);
        }   

        $user = new ClientCredentials(
            $client_Id,
            $client_secret,
            $this->grantType
        );

        return $user;
    }

    public function supportsClass(string $class)
    {
        return ClientCredentials::class === $class || is_subclass_of($class, ClientCredentials::class);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof ClientCredentials) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        
        if(($user->getClientId()!==  $this->clientId) and ($user->getClientId()!==  $this->clientSecret)){
            throw new UserNotFoundException("Credentials", $user->getUsername());
        }
        
        return $user;
    }

}
