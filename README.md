# ApiAuthenticationBundle

Diese Bibliothek Implementiert die API-Authentifizierungsprozesse.

Die Bibliothek ermöglicht zurzeit die Authentifitierung mit:

- API-Key Authentication
- OAuht2- Protokol
    - Client Credential Grant type

## Installation

Fügt die folgen zeile in der `composer.json` Dateie

```
        "repositories": [
        {
            "type": "vcs",
            "url":  "https://git.oskar-campus.eu/Project.SymfonyBundle/ApiAuthenticationBundle.git"
        }
    ]
```

und dann die folgen befehl ausführen `composer req oskar/api-authentication-bundle:*@dev -w`. Setllen sie sich sicher, dass deine Anwendung die Anforderung von diser Bundle erfüllt.

```
        "php": ">=8.0.2",
        "doctrine/doctrine-bundle": "^2.5",
        "doctrine/doctrine-migrations-bundle": "^3.2",
        "doctrine/mongodb-odm": "^2.4",
        "doctrine/mongodb-odm-bundle": "^4.5",
        "gesdinet/jwt-refresh-token-bundle": "^1.1",
        "lexik/jwt-authentication-bundle": "^2.16"
```


## Einstellung

- Eine neue Datei namen `oskar_api_authentication` mit  dieser Einstellung erstellen.

```
    oskar_api_authentication:
    api:
        api_key: '%env(resolve:APP_API_KEY)%'
    oauth2:
        client_id: '%env(resolve:APP_CLIENT_ID)%'
        client_secret: '%env(resolve:APP_CLIENT_SECRET)%'
        grant_type: client_credentials 
```

-  stellen sie sich sicher, dass `gesdinet_jwt_refresh_token` und `lexik_jwt_authentication` `clientId` als **Identication Feld benutzt**.

- Konfigurieren Sie Symfony Security mit `custom_provider`, der auf `oskar_oauth_authentication.client_credentials_provider` Zeigt.

- neue Klasse Erstellen, die die Klasse `JWTAuthenticator` von `Lexik\Bundle\JWTAuthenticationBundle` und als Provider die Klasse 'OauthCredentialProvider' in dem Lonstruktor importieren. Sie können die Methoden überschreiden nach ihren Wünsch.
